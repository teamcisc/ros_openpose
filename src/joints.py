#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from enum import Enum


class Joints(Enum):
    """
    Simple enum, used by detector to gets coordinates for each person
    """
    NOSE = 0
    NECK = 1
    RSHOULDER = 2
    RELBOW = 3
    RWRIST = 4
    LSHOULDER = 5
    LELBOW = 6
    LWRIST = 7