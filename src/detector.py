#!/usr/bin/env python
# -*- coding: utf-8 -*
import getpass

import cv2
import numpy
import rospy
import time
import std_msgs.msg
from openpose import pyopenpose as op
# Import message specification
from ros_openpose.msg import People, Position, Person

from joints import Joints


class Detector:
    def __init__(self):
        # Init node and publisher
        self.pub = rospy.Publisher('ros_openpose/human_pos', People, queue_size=10)
        rospy.init_node('detector', anonymous=True)
        self.rate = rospy.Rate(30)  # rospy.get_param('cisc/rate'))

        # Camera attributes
        self.__cam_index = None
        self.__cam = None

        # To save video
        self.__fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.log_path = ''
        self.out = None
        self.fps = 30

        # Init openpose
        self.params = dict()
        self.params["net_resolution"] = "-1x80"
        self.params["model_pose"] = "BODY_25"
        # self.params["process_real_time"] = True
        self.params["output_resolution"] = "640x480"
        self.params["model_folder"] = "../../../models/"
        self.params["render_pose"] = 1
        self.params["model_folder"] = '/home/{}/openpose/models/'.format(getpass.getuser())

        self.opWrapper = op.WrapperPython()
        self.opWrapper.configure(self.params)
        self.opWrapper.start()

    def run(self):
        """
        Runs the detector.
        It publishes  the number of people and their positions in the analysed frame.
        """
        while not rospy.is_shutdown():
            # Try to open camera or update camera (if the index changed)
            self.__open_camera()

            s, img = self.__cam.read()

            if s:  # Frame captured without any errors
                # Output key points and the image with the human skeleton blended on it
                # human pose key points = [#people x #keypoints x 3]-dimensional numpy object
                if rospy.get_param('cisc/gui/started'):
                    self.__write_vid(rospy.get_param('cisc/ros_openpose/save_video'), img)

                datum = op.Datum()
                datum.cvInputData = img
                self.opWrapper.emplaceAndPop([datum])

                msg = People()
                if not datum.poseKeypoints.all() and datum.poseKeypoints.any():
                    for coordinates in datum.poseKeypoints:
                        pers = Person()
                        for j in Joints:
                            pos = Position()
                            pos.x = coordinates[j.value][0] if coordinates[j.value][0] != -1 else numpy.nan
                            pos.y = coordinates[j.value][1] if coordinates[j.value][1] != -1 else numpy.nan

                            attr = [a for a in dir(pers) if a == j.name][0]
                            pers.__setattr__(attr, pos)
                        msg.people.append(pers)
                # else:
                #     pers = Person()
                #     for j in Joints:
                #         pos = Position
                #     msg.people.append(pers)

                cv2.imshow("output", datum.cvOutputData)
                cv2.waitKey(1)
                msg.header = std_msgs.msg.Header()
                msg.header.stamp = rospy.Time.now()
                self.pub.publish(msg)

        self.stop()

    def __write_vid(self, save_video, img):
        if save_video:
            log_base_folder_path = rospy.get_param('cisc/log_base_folder')
            log_folder_path = rospy.get_param('cisc/log_folder')
            tmp_path = log_base_folder_path + log_folder_path + 'body.avi'
            if self.log_path == tmp_path:
                self.out.write(img)
            else:
                self.log_path = tmp_path
                self.out = cv2.VideoWriter(self.log_path, self.__fourcc, self.fps, (640, 480))
                self.out.write(img)

    def __open_camera(self):
        """
        Loops until a camera is open or the node is shut down
        """
        cam_opened = False
        new_camera_index = rospy.get_param('cisc/ros_openpose/cam_index')
        if new_camera_index != self.__cam_index:
            while not cam_opened and not rospy.is_shutdown():
                self.__cam = cv2.VideoCapture(new_camera_index)
                s, _ = self.__cam.read()
                if not s:
                    cam_opened = False
                    new_camera_index = rospy.get_param('cisc/ros_openpose/cam_index')
                else:
                    cam_opened = True
                    self.__cam_index = new_camera_index
                try:
                    self.fps = self.__cam.get(cv2.cv2.CAP_PROP_FPS)
                except:
                    rospy.log('**[CAMERA] WARNING: I can\'t get the camera fps, so I\'m using a default one.')
                    self.fps = 30
                w = self.__cam.get(cv2.cv2.CAP_PROP_FRAME_WIDTH)
                h = self.__cam.get(cv2.cv2.CAP_PROP_FRAME_HEIGHT)
                rospy.set_param('/cisc/ros_openpose/width', w)
                rospy.set_param('/cisc/ros_openpose/height', h)
                time.sleep(1)

    def stop(self):
        """
        Kill ros_openpose.
        :param msg: useful just to declare a ROS service with empty parameter.
        """
        rospy.loginfo('[ROS_OPENPOSE] Stopping myself.')
        self.opWrapper.stop()
        self.__cam.release()
        cv2.destroyAllWindows()
        return []


def main():
    """
    Main function running the detector.
    :return:
    """
    detector = Detector()
    detector.run()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
